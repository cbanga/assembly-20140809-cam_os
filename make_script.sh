#!/bin/bash

# the flag tells bash to exit if a command fails
set -e

rm -f kernel.img

make

echo -e "Build complete!"